Algoritmo sin_titulo
	Escribir "Introduzca un numero: "
	Leer numero
	
	Escribir "Ingrese 1 para calcular factorial"
	Escribir "Ingrese 2 para saber si el numero es par"
	Escribir "Ingrese 3 para terminar y salir"
	Escribir "Operacion a realizar con el numero ingresado: "
	Leer operacion
	
	
	Segun operacion Hacer
		1:
			contador <- 1
			factorial <- 1
			Mientras contador <= numero Hacer
				factorial <- factorial * contador
				contador <- contador + 1
			Fin Mientras
			Escribir "El factorial es: ", factorial
		2:
			Si numero % 2 = 0 Entonces
				Escribir "El numero es par"
			SiNo
				Escribir "El numero es impar"
			FinSi
		3:
			Escribir "Saliendo del programa...."
		De Otro Modo:
			Escribir "Operacion no valida"
	Fin Segun
FinAlgoritmo
