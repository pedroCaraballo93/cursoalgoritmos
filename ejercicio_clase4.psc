Algoritmo vectores
	// determinacion de cantidad de numeros a leer
	Escribir "Cantidad de numeros a leer: "
	Leer cantidad
	Dimension  num[cantidad]
	suma_impares <- 0
	prom_pares <- 0
	contador_pares <- 0
	
	// captura de los numeros
	Para i<-1 Hasta cantidad Con Paso 1 Hacer
		Escribir "Ingrese un numero para la posici�n ",i 
		Leer num[i]
	FinPara
	
	// operacion sobre los numeros ingresados en el vector
	Para i<-1 Hasta cantidad Con Paso 1 Hacer
		
		Si  num[i] % 2 = 0 Entonces
			prom_pares <- prom_pares + num[i]
			contador_pares <- contador_pares + 1
		SiNo
			suma_impares <- suma_impares + num[i]
		Fin Si
		
		// determinar el menor
		
		Si i = 1 Entonces
			numero_menor <- num[i]
		SiNo
			Si num[i] < numero_menor Entonces
				numero_menor <- num[i]
			FinSi
		FinSi
		
	FinPara
	
	prom_pares <- prom_pares / contador_pares
	
	// impresion de resultados
	Escribir "La suma de los impares es: ", suma_impares
	Escribir "El promedio de los pares es: ", prom_pares
	Escribir "El menor es el numero: ", numero_menor
	
FinAlgoritmo
