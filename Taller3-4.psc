Algoritmo RendondearNumero
	Escribir "Ingrese el numero"
	Leer numero
	Si numero < 1000 Entonces
		Escribir "El numero debe ser mayor o igual a 1000"
	SiNo
		Si numero % 1000 > 499 Entonces
			Escribir "Se debe redondear hacia arriba: " numero + (1000 - (numero % 1000))
		SiNo
			Si numero % 1000 == 0 Entonces
				Escribir "El numero redondeado es igual al original: ", numero
			SiNo
				Escribir "Se debe redondear hacia abajo: " numero - (numero % 1000)
			Fin Si
			
		Fin Si
	Fin Si
FinAlgoritmo
