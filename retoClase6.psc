Algoritmo sin_titulo

	Dimension matriz[9, 5]
	
	matriz[1, 1] <- "------"
	matriz[1, 2] <- "Regi�n Caribe"
	matriz[1, 3] <- "Regi�n Andina"
	matriz[1, 4] <- "Eje cafetero"
	matriz[1, 5] <- "Llanos orientales"
	
	// azar(1000000))
	matriz[2, 1] <- "Claro--- Internet"
	matriz[2, 2] <- ConvertirATexto(azar(1000000)) // genera numeros aleatorios de 1 a 1000000
	matriz[2, 3] <- ConvertirATexto(2000)
	matriz[2, 4] <- ConvertirATexto(3000)
	matriz[2, 5] <- ConvertirATexto(4000)
	
	matriz[3, 1] <- "Claro--- Telefonia"
	matriz[3, 2] <- ConvertirATexto(5000)
	matriz[3, 3] <- ConvertirATexto(5500)
	matriz[3, 4] <- ConvertirATexto(6000)
	matriz[3, 5] <- ConvertirATexto(1000)
	
	matriz[4, 1] <- "ETB----- Internet"
	matriz[4, 2] <- ConvertirATexto(1000)
	matriz[4, 3] <- ConvertirATexto(2000)
	matriz[4, 4] <- ConvertirATexto(3000)
	matriz[4, 5] <- ConvertirATexto(4000)
	
	matriz[5, 1] <- "ETB----- Telefonia"
	matriz[5, 2] <- ConvertirATexto(0)
	matriz[5, 3] <- ConvertirATexto(500)
	matriz[5, 4] <- ConvertirATexto(6000)
	matriz[5, 5] <- ConvertirATexto(1000)
	
	matriz[6, 1] <- "Movistar Internet"
	matriz[6, 2] <- ConvertirATexto(1000)
	matriz[6, 3] <- ConvertirATexto(1000)
	matriz[6, 4] <- ConvertirATexto(2000)
	matriz[6, 5] <- ConvertirATexto(3000)
	
	matriz[7, 1] <- "Movistar Telefonia"
	matriz[7, 2] <- ConvertirATexto(500)
	matriz[7, 3] <- ConvertirATexto(500)
	matriz[7, 4] <- ConvertirATexto(1000)
	matriz[7, 5] <- ConvertirATexto(1000)
	
	matriz[8, 1] <- "Tigo-UNE Internet"
	matriz[8, 2] <- ConvertirATexto(10000)
	matriz[8, 3] <- ConvertirATexto(10000)
	matriz[8, 4] <- ConvertirATexto(20000)
	matriz[8, 5] <- ConvertirATexto(3000)
	
	matriz[9, 1] <- "Tigo-UNE Telefonia"
	matriz[9, 2] <- ConvertirATexto(100)
	matriz[9, 3] <- ConvertirATexto(500)
	matriz[9, 4] <- ConvertirATexto(10)
	matriz[9, 5] <- ConvertirATexto(0)
	
	
	// imprimir la matriz
	Para fila<-1 Hasta 9 Con Paso 1 Hacer
		
		Escribir " | ", matriz[fila, 1], " | ", matriz[fila, 2], " | ", matriz[fila, 3], " | ", matriz[fila, 4], " | ", matriz[fila, 5], " | "
		
	Fin Para
	
	Escribir "----------------------------------------------------------------------------"
	
	// responder preguntas
	contador <- 0
	contador_filas <- 0
	suma_fila <- 0
	operador_mayor <- matriz[2, 1]
	multa_mayor <- 0
	
	Para fila<-2 Hasta 9 Con Paso 1 Hacer
		Para columna<-2 Hasta 5 Con Paso 1 Hacer
			
			Si contador < 8 Entonces
				suma_fila <- suma_fila + ConvertirANumero(matriz[fila, columna])
				contador <- contador + 1
			FinSi
			
		Fin Para
		
		contador_filas <- contador_filas + 1
		
		Si contador_filas = 2 Entonces
			
			// pregunta 1: total de multas por cada operador
			Escribir "Las multas del operador ", subcadena(matriz[fila, 1],1,8), " son ", suma_fila
			
			Si suma_fila > multa_mayor Entonces
				multa_mayor <- suma_fila
				operador_mayor <- matriz[fila, 1]
			FinSi
			
			suma_fila <- 0
			contador <- 0
			contador_filas <- 0
			
		Fin Si
		
	Fin Para
	
	// pregunta 2: operador con mas multas
	Escribir "El operador con mas multas es ", subcadena(operador_mayor,1,8)
	
	// pregunta 3: region con menos multas
	
	region_menor <- ""
	contador2 <- 0
	suma_columna <- 0
	multa_menor <- 0
	
	Para columna<-2 Hasta 5 Con Paso 1 Hacer
		
		// -----------------
		//regiones_internet[columna - 1, 1 ] <- matriz[1, columna]
		
		
		Para fila<-2 Hasta 9 Con Paso 1 Hacer
			
			Si contador2 < 8 Entonces
				suma_columna <- suma_columna + ConvertirANumero(matriz[fila, columna])
				contador2 <- contador2 + 1
			FinSi
			
		Fin Para
		
		Si region_menor = "" O suma_columna < multa_menor Entonces
			
			multa_menor <- suma_columna
			region_menor <- matriz[1, columna]
			
		FinSi
		
		suma_columna <- 0
		contador2 <- 0
		
	Fin Para
	
	Escribir "La regi�n con menos multas es ", region_menor

	// pregunta 3: promedio de multas en el eje cafetero
	
	suma_columna <- 0
	contador3 <- 0
	
	Para columna<-4 Hasta 4 Con Paso 1 Hacer
		
		Para fila<-2 Hasta 9 Con Paso 1 Hacer
			
				suma_columna <- suma_columna + ConvertirANumero(matriz[fila, columna])
				contador3 <- contador3 + 1
			
		Fin Para
		
	Fin Para
	
	Escribir "El promedio de las multas en el eje cafetero es ", suma_columna / contador3

	// pregunta 5: Las dos regiones con menos multas por prestaci�n del servicio de Internet Banda Ancha 
	
	region_menor1 <- ""
	region_menor2 <- ""
	contador2 <- 0
	suma_columna <- 0
	multa_menor1 <- 0
	multa_menor2 <- 0
	
	Para columna<-2 Hasta 5 Con Paso 1 Hacer
	
		Para fila<-2 Hasta 9 Con Paso 1 Hacer
			
			Si contador2 < 4 Y fila % 2 = 0 Entonces
				suma_columna <- suma_columna + ConvertirANumero(matriz[fila, columna])
				contador2 <- contador2 + 1
			FinSi
			
		Fin Para
		
		Escribir "la suma de la region ", matriz[1, columna], " es: ", suma_columna
		
		// asignando los valores menoes iniciales
		Si region_menor1 = "" Entonces
			region_menor1 <- matriz[1, columna]
			multa_menor1 <- suma_columna
		FinSi
		
		Si region_menor2 = "" Y region_menor1 <> "" O region_menor2 <> "" Y region_menor1 <> "" Entonces
			
			Si region_menor2 = "" Entonces
				region_menor2 <- matriz[1, columna]
			FinSi
			
			
			Si suma_columna < multa_menor1 Y suma_columna < multa_menor2 Entonces
				
				aux <- multa_menor1
				aux2 <- region_menor1
				
				// actualizo los valores
				multa_menor1 <- suma_columna
				multa_menor2 <- aux
				
				// actualizo la region
				region_menor1 <- matriz[1, columna]
				region_menor2 <- aux2
			SiNo

					multa_menor2 <- suma_columna
					region_menor2 <- matriz[1, columna]
					
				
			FinSi
			
			
			
		FinSi
		
		suma_columna <- 0
		contador2 <- 0
		
	Fin Para
	
	Escribir "La region 1 menor es: ", region_menor1
	Escribir "La region 2 menor es: ", region_menor2
	
FinAlgoritmo
