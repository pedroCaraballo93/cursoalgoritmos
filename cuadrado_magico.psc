// imprimir el tablero de juego
funcion imprimir_matriz(matriz)
	
	Para fila <- 1 Hasta 3 Con Paso 1 Hacer
		Escribir " | ", matriz[fila, 1], " | ", matriz[fila, 2], " | ", matriz[fila, 3], " | "
	FinPara
finfuncion

// funcion para validar si se gana o pierde el juego
funcion validar_matriz(matriz)
	
	// verificando suma de filas
	suma_fila1 <- 0
	suma_fila2 <- 0
	suma_fila3 <- 0
	Para fila <- 1 Hasta 3 Con Paso 1 Hacer
		Para columna <- 1 Hasta 3 Con Paso 1 Hacer
			
			Segun fila Hacer
				1:
					suma_fila1 <- suma_fila1 + matriz[1, columna]
				2:
					suma_fila2 <- suma_fila2 + matriz[2, columna]
				De Otro Modo:
					suma_fila3 <- suma_fila3 + matriz[3, columna]
			Fin Segun
			
		FinPara
	FinPara
	
	// verificando suma de columnas
	suma_col1 <- 0
	suma_col2 <- 0
	suma_col3 <- 0
	Para columna <- 1 Hasta 3 Con Paso 1 Hacer
		Para fila <- 1 Hasta 3 Con Paso 1 Hacer
			
			Segun columna Hacer
				1:
					suma_col1 <- suma_col1 + matriz[fila, 1]
				2:
					suma_col2 <- suma_col2 + matriz[fila, 2]
				De Otro Modo:
					suma_col3 <- suma_col3 + matriz[fila, 3]
			Fin Segun
			
		FinPara
	FinPara
	
	Si suma_fila1 = 15 Y suma_fila2 = 15 Y suma_fila3 = 15 Y suma_col1 = 15 Y suma_col2 = 15 Y suma_col3 = 15 Entonces
		Escribir "---------Ha ganado el juego------------"
	SiNo
		Escribir "---------Upps has cometido un error------------"
	Fin Si
	
finfuncion


Algoritmo cuadradoMagico
	
	Dimension cuadrado[3, 3] // filas vs columnas
	contador <- 0
	continuar <- 11
	
	// inicializo en 0 la matriz
	Para fila <- 1 Hasta 3 Con Paso 1 Hacer
		Para columna <- 1 Hasta 3 Con Paso 1 Hacer
		
			cuadrado[fila, columna] <- 0
		
		Fin Para
	Fin Para
	
	// pido datos
	
	Mientras contador < 9 Y continuar = 11 Hacer
		
		// imprimir la matriz en su forma inicial
		Si contador = 0 Entonces
			imprimir_matriz(cuadrado)
		Fin Si
		
		Escribir "Fila para el numero: ", contador + 1
		Leer fila
		
		Escribir "Columna para el numero: ", contador + 1
		Leer columna
		
		Escribir "---------------------------------------"
		
		// valido que la posicion sea valida
		Si fila < 1 O fila > 3 O columna < 1 O columna > 3 Entonces
			Escribir "La posicion ingresada no existe, ingrese una posici�n valida"
		SiNo
			// verifico si la celda esta ocupada
			Si cuadrado[fila, columna] = 0 Entonces
				
				cuadrado[fila, columna] <- contador + 1
				contador <- contador + 1
				imprimir_matriz(cuadrado)
				
				
				Si contador < 9 Entonces
					
					Escribir "�Continuar el juego? (11 = si, 0 = no): "
					Leer resp
					
					Si resp = 11 O resp = 0 Entonces
						continuar <- resp
					SiNo
						Escribir "Respuesta no valida, el juego continua"
					Fin Si
					
				SiNo
					validar_matriz(cuadrado)
				Fin Si
				
			SiNo
				Escribir "La celda seleccionada esta ocupada"
			Fin Si
		Fin Si
	Fin Mientras
	
FinAlgoritmo
