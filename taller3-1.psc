Algoritmo sin_titulo
	cantidad_personas <- 3
	contador <- 0
	condicion_simultanea <- 0
	prom_edad <- 0
	prom_estatura <- 0
	
	Repetir
		Escribir "Ingrese la edad: "
		Leer edad
		
		Escribir "Estado civil (1 = Soltero, 2 = Casado): "
		Leer estado
		
		Escribir "Estatura: "
		Leer estatura
		
		Escribir "Sexo (1 = Hombre, 2 = Mujer): "
		Leer sexo
		
		Si edad > 0 Y estatura > 0 Y (estado = 1  O estado = 2) Y (sexo = 1 O sexo = 2) Entonces
			
			Escribir "Valido"
			
			// verificamos si se cumple la condicion_simultanea
			Si edad >= 18 Y estado = 1 Y sexo = 1 Y estatura > 1.70 Entonces
				condicion_simultanea <- condicion_simultanea + 1
				prom_edad <- prom_edad + edad
				prom_estatura <- prom_estatura + estatura 
			Fin Si
			
		SiNo
			Escribir "No Valido"
		Fin Si
		
	
		contador <- contador + 1
	Hasta Que contador == cantidad_personas
	
	// mostrar resultados
	porcentaje <- (condicion_simultanea / cantidad_personas) * 100
	prom_edad <- prom_edad / condicion_simultanea
	prom_estatura <- prom_estatura / condicion_simultanea
	
	Escribir "La condici�n simultanea se cummple: ", condicion_simultanea, " veces"
	Escribir "Porcentaje: ", porcentaje, "%"
	Escribir "Promedio de edad: ", prom_edad
	Escribir "Promedio de estatura: ", prom_estatura
FinAlgoritmo

