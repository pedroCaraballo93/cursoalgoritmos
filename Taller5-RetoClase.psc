Algoritmo trabajadores
	
	Dimension  nombres[5]
	nombres[1] = "Luis"
	nombres[2] = "Pedro"
	nombres[3] = "Manuel"
	nombres[4] = "Armando"
	nombres[5] = "Juan"
	
	Dimension matriz[2, 7]
	contador <- 1
	
	// recambiar el numero de filas aqui y arriba
	Para fila <- 1 Hasta 2 Con Paso 1 Hacer
		
		// lectura de la matriz
		Para columna <- 1 Hasta 7 Con Paso 1 Hacer
			nombre_columna <- ""
			
			Segun columna Hacer
				1:
					nombre_columna <- "lunes"
				2:
					nombre_columna <- "martes"
				3:
					nombre_columna <- "miercoles"
				4:
					nombre_columna <- "jueves"
				5:
					nombre_columna <- "viernes"
				6:
					nombre_columna <- "sabado"
				De Otro Modo:
					nombre_columna <- "Valor/hora"
			Fin Segun
			
			Escribir "Ingrese ", nombre_columna, " para ", nombres[fila], ": "
			Leer matriz[fila, columna]
		
		Fin Para
		
	Fin Para
	
	// horas trabajadas por persona
	suma_fila <- 0
	total_pagar_semana <- 0
	nombre_menor <- nombres[1]
	horas_menor <- 0
	Para fila <- 1 Hasta 2 Con Paso 1 Hacer
		Para columna <- 1 Hasta 6 Con Paso 1 Hacer
			
			suma_fila <- suma_fila + matriz[fila, columna]
			
			Si fila = 1 Entonces
				horas_menor <- matriz[fila, 6]
			SiNo
				
				Si matriz[fila, 6] < horas_menor Entonces
					
					nombre_menor <- nombres[fila]
					horas_menor <- matriz[fila, 6]
				Fin Si
				
			Fin Si

		FinPara
		Escribir "Las horas trabajadas por ", nombres[fila], " en la semana fueron: ", suma_fila
		Escribir "El sueldo semanal de ", nombres[fila], " es de : ", suma_fila * matriz[fila, 7]
		total_pagar_semana <- total_pagar_semana + suma_fila * matriz[fila, 7]
		suma_fila <- 0
		
	FinPara
	
	Escribir "El total a pagar por la empresa semanalmente es de: ", total_pagar_semana
	Escribir "El trabajador que labor� menos horas el viernes fue: ", nombre_menor
	
FinAlgoritmo
