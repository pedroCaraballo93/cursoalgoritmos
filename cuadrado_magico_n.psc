// imprimir el tablero de juego
funcion imprimir_matriz(matriz, dimensiones)
	
	contador <- 0
	barra <- " | "
	cadena <- barra
	Para fila <- 1 Hasta dimensiones Con Paso 1 Hacer
		Para columna <- 1 Hasta dimensiones Con Paso 1 Hacer
			
			Si contador < dimensiones Entonces
				
				cadena <- CONCATENAR(cadena, convertirATexto(matriz[fila, columna]))
				cadena <- CONCATENAR(cadena, barra)
				contador <- contador + 1

			Fin Si
	
		FinPara 
		
		Escribir cadena
		contador <- 0
		cadena <- barra
	FinPara
finfuncion

// funcion para validar si se gana o pierde el juego
funcion validar_matriz(matriz, dimensiones)
	
	// --------------------------------calculando suma de filas
	Dimension vector_sum[dimensiones]
	suma_fila <- 0
	contador <- 0
	
	Para fila <- 1 Hasta dimensiones Con Paso 1 Hacer
		Para columna <- 1 Hasta dimensiones Con Paso 1 Hacer
			
			Si contador < dimensiones Entonces
				suma_fila <- suma_fila + matriz[fila, columna]
				contador <- contador + 1
			Fin Si
			
		FinPara
		
		vector_sum[fila] <- suma_fila
		contador <- 0
		suma_fila <- 0
	FinPara
	
	// ----------------------------------calculando suma de columnas
	Dimension vector_sum2[dimensiones]
	suma_columna <- 0
	contador2 <- 0
	
	Para columna <- 1 Hasta dimensiones Con Paso 1 Hacer
		Para fila <- 1 Hasta dimensiones Con Paso 1 Hacer
			
			Si contador < dimensiones Entonces
				suma_columna <- suma_columna + matriz[fila, columna]
				contador <- contador + 1
			Fin Si
			
		FinPara
		
		vector_sum2[columna] <- suma_columna
		contador <- 0
		suma_columna <- 0
	FinPara
	
	// -----------------------------verificando suma igual de filas y columnas
	inicio <- vector_sum[1]
	resp = 1
	contador_ok <- 0
	contador_error <- 0
	Para celda <- 2 Hasta dimensiones Con Paso 1 Hacer
		
		Si vector_sum[celda] <> inicio O vector_sum2[celda] <> inicio  Entonces
			contador_error <- contador_error + 1
		SiNo
			contador_resp <- contador_resp + 1
		Fin Si
		
	FinPara
	
	Si contador_error > 0 Entonces
		Escribir "------Upps ha cometido un error--------"
	SiNo
		Escribir "---------Ha ganado el juego------------"
	Fin Si
	
	
finfuncion


Algoritmo cuadradoMagicoN
	
	Escribir "Digite el numero de filas y columnas: "
	Leer dimensiones
	
	Si dimensiones >= 3 Y dimensiones % 2 > 0 Entonces
		
		Dimension cuadrado[dimensiones, dimensiones] // filas vs columnas
		contador <- 0
		
		// inicializo en 0 la matriz
		Para fila <- 1 Hasta dimensiones Con Paso 1 Hacer
			Para columna <- 1 Hasta dimensiones Con Paso 1 Hacer
				
				cuadrado[fila, columna] <- 0
				
			Fin Para
		Fin Para
		
		celdas <- dimensiones * dimensiones
		
		// pido los datos
		Mientras contador < celdas Hacer
		
			//imprimir la matriz en su forma inicial
			Si contador = 0 Entonces
				imprimir_matriz(cuadrado, dimensiones)
			Fin Si
			
			Escribir "Fila para el numero: ", contador + 1
			Leer fila
			
			Escribir "Columna para el numero: ", contador + 1
			Leer columna
			
			Escribir "---------------------------------------"
			
			// valido que la posicion sea valida
			Si fila < 1 O fila > dimensiones O columna < 1 O columna > dimensiones Entonces
				Escribir "La posicion ingresada no existe, ingrese una posici�n valida"
			SiNo
				// verifico si la celda esta ocupada
				Si cuadrado[fila, columna] = 0 Entonces
					
					cuadrado[fila, columna] <- contador + 1
					contador <- contador + 1
					imprimir_matriz(cuadrado, dimensiones)
					
					Si contador = celdas Entonces
						
						validar_matriz(cuadrado, dimensiones)
						
					Fin Si
					
				SiNo
					Escribir "La celda seleccionada esta ocupada"
				Fin Si
			Fin Si	
		Fin Mientras
		
	SiNo
		Escribir "Las dimensiones deben ser impares de 3 en adelante"
	Fin Si
	
FinAlgoritmo
